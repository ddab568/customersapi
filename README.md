# Customers API #

* Lists all customers
* Retrieves specific customers using ID
* Deletes specific customers using ID
* Patches/Updates specific customers using ID

Demo can be seen here: 
http://danasixtree.cloudhub.io/api/customers